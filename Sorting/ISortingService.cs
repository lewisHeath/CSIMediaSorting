﻿namespace CSIMedia.Sorting
{
    public interface ISortingService
    {
        public int[] sortAsc(int[] integers);

        public int[] sortDesc(int[] integers);
    }
}

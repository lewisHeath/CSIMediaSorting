﻿using System.ComponentModel.DataAnnotations;

namespace CSIMedia.Models
{
    public class Sort
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]

        public string numbers { get; set; }
        [Required]

        public double timeTaken { get; set; }
        [Required]

        public string order { get; set; }
        [Required]

        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }
}

﻿
namespace CSIMedia.Models
{
    public class JsonSort
    {
        public int Id { get; set; }


        public int[] numbers { get; set; }


        public double timeTaken { get; set; }


        public string order { get; set; }


        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }
}

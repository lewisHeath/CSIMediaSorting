﻿using CSIMedia.Data;
using CSIMedia.Models;
using CSIMedia.Sorting;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CSIMedia.Controllers
{
    public class SortController : Controller
    {
        private readonly ApplicationDbContext _database;
        private readonly ISortingService _sortingService;

        public SortController(ApplicationDbContext database, ISortingService sortingService)
        {
            _database = database;
            _sortingService = sortingService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(Sort sequence)
        {
            if(ModelState.IsValid)
            {
                // Main logic 
                /*
                 * 1. Validate the sequence of numbers is comma separated and create an array
                 * 2. Get sorting order
                 * 3. Sort numbers
                 * 4. Create a Sort object and store in database
                 */

                var sequenceStrings = sequence.numbers.Split(',');
                var integers = new int[sequenceStrings.Length];

                // Try to parse each number into an int type
                for (int i = 0; i < sequenceStrings.Length; i++)
                {
                    if (int.TryParse(sequenceStrings[i], out int num))
                    {
                        integers[i] = num;
                    } else
                    {
                        ModelState.AddModelError("CustomError", "Invalid number sequence format");
                        return View(sequence);
                    }
                }

                // Get the sorting order
                var sortingOrder = sequence.order.ToString();
                // Start the timer
                Stopwatch sw = Stopwatch.StartNew();

                if(sortingOrder == "Ascending")
                {
                    // Sort into ascending order
                    integers = _sortingService.sortAsc(integers);
                }
                else
                {
                    // Sort into descending order
                    integers = _sortingService.sortDesc(integers);
                }
                // Stop the timer
                var ms = sw.Elapsed.TotalMilliseconds;
                sw.Stop();

                // Update the Sort object
                sequence.numbers = string.Join(",", integers);
                sequence.timeTaken = ms;

                // Add to the database
                await _database.Sorts.AddAsync(sequence);
                await _database.SaveChangesAsync();

                TempData["success"] = "Sequence created successfully";

                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}

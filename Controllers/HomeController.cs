﻿using CSIMedia.Data;
using CSIMedia.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;

namespace CSIMedia.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _database;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext database)
        {
            _logger = logger;
            _database = database;

        }

        public IActionResult Index()
        {
            var sorts = _database.Sorts;
            return View(sorts);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var sort = _database.Sorts.Find(id);
            _database.Sorts.Remove(sort);
            await _database.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public IActionResult JSON()
        {
            // Download database as JSON. TODO
            var sorts = _database.Sorts.ToList();
            // Convert to JSON format
            var jsonSorts = createJsonSorts(sorts);
            var jsonContent = JsonConvert.SerializeObject(jsonSorts, Formatting.Indented);
            var jsonBytes = System.Text.Encoding.UTF8.GetBytes(jsonContent);
            return File(jsonBytes, "application/json", "Sorts.json");
        }

        private List<JsonSort> createJsonSorts(List<Sort> sorts)
        {
            // linq 
            var jsonSorts = sorts.Select(sort => new JsonSort
            {
                Id = sort.Id,
                numbers = sort.numbers.Split(',').Select(numberString =>
                {
                    if (int.TryParse(numberString, out int number))
                    {
                        return number;
                    } else
                    {
                        //defualt case can be 0 but this shouldnt happen because it was validated before entering database
                        return 0;
                    }
                }).ToArray(),
                timeTaken = sort.timeTaken,
                order = sort.order,
                CreatedDateTime = sort.CreatedDateTime
            }).ToList();

            return jsonSorts;
        }

        
    }
}
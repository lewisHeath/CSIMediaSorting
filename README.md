﻿
# CSI Media Coding Test

This is my submission for the coding test set by CSI Media

Please make sure to amend the connection string for the database in the ```appsettings.json``` file.

I am using Entity Framework, so make sure to run ```dotnet ef database update``` to create the database.

To host the application please run ```dotnet run```.


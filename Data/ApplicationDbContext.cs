﻿using Microsoft.EntityFrameworkCore;
using CSIMedia.Models;

namespace CSIMedia.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }

        public DbSet<Sort> Sorts { get; set; }
    }
}
